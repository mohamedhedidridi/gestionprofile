<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ModuleController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\MenuSideBarController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect('/dashboard');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('home');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::group(['prefix' => 'admin','App\Http\Controllers' => 'MenuSideBarController', 'middleware' => ['auth']], function () {
    Route::get('/MenuSideBar', [MenuSideBarController::class, 'index'])->name('admin.MenuSideBar.index')->middleware('permission:View All Menu');
    Route::POST('/MenuSideBar/store', [MenuSideBarController::class , 'store'])->name('admin.MenuSideBar.store')->middleware('permission:Add Menu');
    Route::get('/MenuSideBar/ajax/{id}',[MenuSideBarController::class , 'ajaxParentPermission'])->name('admin.MenuSideBar.ajaxParentPermission');
});

Route::group(['prefix' => 'admin','App\Http\Controllers' => 'ModuleController', 'middleware' => ['auth']], function () {
    Route::get('/modules', [ModuleController::class, 'index'])->name('admin.modules.index')->middleware('permission:View All Modules');
    Route::POST('/modules/store', [ModuleController::class , 'store'])->name('admin.modules.store')->middleware('permission:Add Module');
});

Route::group(['prefix' => 'admin','App\Http\Controllers' => 'PermissionController', 'middleware' => ['auth']], function () {
    Route::get('/permissions', [PermissionController::class, 'index'])->name('admin.permissions.index')->middleware('permission:View All Permissions');
    Route::POST('/permissions/store', [PermissionController::class , 'store'])->name('admin.permissions.store')->middleware('permission:Add permission');
});

Route::group(['prefix' => 'admin','App\Http\Controllers' => 'RoleController', 'middleware' => ['auth']], function () {
    Route::get('/roles', [RoleController::class, 'index'])->name('admin.roles.index')->middleware('permission:View All Roles');
    Route::POST('/roles/store', [RoleController::class , 'store'])->name('admin.roles.store')->middleware('permission:Add role');
    Route::get('/roles/{role}/edit', [RoleController::class, 'edit'])->name('admin.roles.edit')->middleware('permission:Attribut permissions');
    Route::post('/roles/attribuer/{role}',[RoleController::class,'attribuer'])->name('admin.roles.attribuer')->middleware('permission:Attribut permissions');
});

Route::group(['prefix' => 'admin','App\Http\Controllers' => 'UserController', 'middleware' => ['auth']], function () {
    Route::get('/users', [UserController::class, 'index'])->name('admin.users.index')->middleware('permission:View All Users');
    Route::get('/users/create', [UserController::class, 'create'])->name('admin.users.create')->middleware('permission:Add User');
    Route::POST('/users/store', [UserController::class , 'store'])->name('admin.users.store')->middleware('permission:Add User');
    Route::get('/users/{id}/edit', [UserController::class, 'edit'])->name('admin.users.edit')->middleware('permission:Update User');
    Route::POST('/users/{id}/update', [UserController::class , 'update'])->name('admin.users.update')->middleware('permission:View Update User');
    Route::get('/users/{id}/reset', [UserController::class , 'resetPassword'])->name('admin.users.reset')->middleware('permission:Update Password');
    Route::get('/user/newPassword',[UserController::class , 'NewPassword'])->name('admin.user.newPassword');
    Route::post('/users/changePassword', [UserController::class, 'changePassword'])->name('admin.users.changePassword');
});

require __DIR__.'/auth.php';
