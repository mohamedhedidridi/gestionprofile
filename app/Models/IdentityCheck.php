<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IdentityCheck extends Model
{
    use HasFactory;

    protected $table = 'identity_check';
}
