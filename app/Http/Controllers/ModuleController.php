<?php

namespace App\Http\Controllers;

use App\Models\Module;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class ModuleController extends Controller
{
    public function index(){
        $titre = "Gestion des Modules";
        $modules = Module::all();
        return view('admin.modules.create', compact('titre','modules'));
    }

    public function store(Request $request){
        try {
            $validator = $request->validate(
                ['name'=>['required','min:3']]
            );
            $module = Module::where('name',strtolower($request->name))->first();
            if($module){
                return redirect()->back()->with( ['status' => 'error' , 'message' => 'Module déjà existe'] );
            }
            $module = new Module;
            $module->name = $request->name ;
            $module->actif = 1;
            if ($module->save()) {
                $permission = new Permission ();
                $permission->name = $module->name;
                $permission->guard_name = "web";
                $permission->module_id = $module->id;
                $permission->save();
                return redirect()->back()->with( ['status' => 'success' , 'message' => 'Module créé avec succès'] );
            }
        } catch (\Exception $th) {
            return redirect()->back()->with( ['status' => 'error' , 'message' => $th->getMessage()] );
        }

    }
}
