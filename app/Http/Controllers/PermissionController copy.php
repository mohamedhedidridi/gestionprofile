<?php

namespace App\Http\Controllers;

use App\Models\Module;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;

class PermissionController extends Controller
{
    public function index(){
        $titre = "Permissions Par Module";
        //$permissions = Permission::groupBy('id_module')->get();
        $modules = Module::where('actif',1)->get();
        return view('admin.permissions.create', compact('titre','modules'));
    }

    public function create(){

    }

    public function store(Request $request){
        try {
            $validator = $request->validate(
                ['name'=>['required','min:3'],
                 'module_id'=>'required','min:1']
            );
            $permission = Permission::where('name',strtolower($request->name))->where('module_id',$request->module)->first();
            if($permission){
                return redirect()->back()->with( ['status' => 'error' , 'message' => 'Permission déjà existe'] );
            }
            $permission = Permission::create($validator);
            $role = Role::where('name','admin')->first();
            $permission->assignRole($role);
            return redirect()->back()->with( ['status' => 'success' , 'message' => 'Permission créé avec succès'] );
        } catch (\Throwable $th) {
            return redirect()->back()->with( ['status' => 'error' , 'message' => $th->getMessage()] );
        }


    }
}
