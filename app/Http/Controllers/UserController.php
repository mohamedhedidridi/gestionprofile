<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\User;
use App\Models\Direction;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{
    public function index(){
        $users = User::all();
        $titre = "Liste des Utilisateur";
        return view('admin.users.index',compact('titre','users'));
    }

    public function create(){
        $titre = 'Créer un nouvel utilisateur';
        $directions = Direction::all();
        $roles = Role::all();
        return view('admin.users.create',compact('titre','roles','directions'));
    }

    public function store(Request $request){
        try {
            $validator = $request->validate(
                ['name'=>['required','min:3'],
                'email'=>['required','min:10','email','unique:users,email'],
                'telephone'=>['required','min:8','max:8','numeric'],
                'role'=>['required','min:1'],
                'direction'=>['required','min:1']
                ]
            );
            $user = User::where('email', $request->email)->first();
            if($user == null){
                $role = Role::find($request->role);
                $user = new User();
                $user->name = $request->name;
                $user->email = $request->email;
                $user->email_verified_at = now();
                $user->password = Hash::make($request->telephone);
                $user->telephone=$request->telephone;
                $user->remember_token = Str::random(10);
                $user->direction_id =$request->direction;
                $user->assignRole($role);
                if ($user->save()) {
                    return redirect()->route('admin.users.index')->with( ['status' => 'success' , 'message' => 'Utilisateur créé avec succès'] );
                }
            }else{
                return redirect()->back()->with( ['status' => 'error' , 'message' => 'Utilisateur déjà existe'] );
            }

        } catch (\Exception $th) {
            return redirect()->back()->with( ['status' => 'error' , 'message' => $th->getMessage()."1"] );

        }
    }

    public function edit($id){
        $user = User::find($id);
        $titre = "Edit User";
        $roles = Role::all();
        return view('admin.users.edit',compact('user','titre','roles'));
    }

    public function update(Request $request, $id){
        try {
            $validator = $request->validate(
                ['name'=>['required','min:3'],
                'telephone'=>['required','numeric'],
                'role'=>['required','min:1'],
                'direction'=>['required','min:1']
                ]
            );
            $user = User::find($id);
            $role = Role::find($request->role);
            $user->name = $request->name;
            $user->telephone = $request->telephone;
            $user->direction_id = $request->direction;
            $user->statut = $request->statut;
            if($user->save()){
                $user->syncRoles($role);
                return redirect()->route('admin.users.index')->with( ['status' => 'success' , 'message' => 'Utilisateur Update avec succès'] );
            }
        } catch (\Exception $th) {
            return redirect()->back()->with( ['status' => 'error' , 'message' => $th->getMessage()."1"] );
        }
    }

    public function show(Request $request , User $user){
        //dd($user);
    }

    public function resetPassword($id){
        try {
            $user = User::find($id);
            $user->password = Hash::make($user->telephone);
            if ($user->save()) {
                return redirect()->route('admin.users.index')->with( ['status' => 'success' , 'message' => 'Mote de passe reset avec succès'] );
            }
        } catch (\Throwable $th) {
            return redirect()->route('admin.users.index')->with( ['status' => 'error' , 'message' => $th->getMessage()] );
        }
    }

    public function changePassword(Request $request)
    {
        try {
            $validator = $request->validate(
                ['Ancien'=>['required','min:8'],
                'Nouveau'=>['required','min:8'],
                'ConfirmNouveau'=>['required','min:8']
                ]
            );
            $Ancien = $request->Ancien;
            $Nouveau = $request->Nouveau;
            $ConfirmNouveau = $request->ConfirmNouveau;
            $user = User::find(Auth::user()->id);
            $test = $Ancien != $user->password;
            if($Nouveau != $ConfirmNouveau){
                return redirect()->back()->with( ['status' => 'error' , 'message' => 'Le Noveau Mot de passe et le Confirm mot de passe ne sont pas identiques'] );
            }elseif(!Hash::check($Ancien , Auth::user()->password)){
                return redirect()->back()->with( ['status' => 'error' , 'message' => 'Ancien Mot de passe incorrect'] );
            }else{
                $user->password = Hash::make($Nouveau);
                if ($user->save()) {
                    return redirect()->back()->with( ['status' => 'error' , 'message' => '"Mot de passe Modifier Avec Succés"'] );
                }
            }
        } catch (\Throwable $th) {
            return redirect()->back()->with( ['status' => 'error' , 'message' => $th->getMessage()] );
        }
    }

    public function NewPassword(){
        $titre ="Paramètres du compte";
        return view('admin.users.changePassword',compact('titre'));
    }
}
