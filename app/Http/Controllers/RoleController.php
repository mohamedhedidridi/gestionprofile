<?php

namespace App\Http\Controllers;

use App\Models\Module;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class RoleController extends Controller
{
    public function index(){
        $titre = "Rôles et permissions";
        $roles = Role::orderBy('id','desc')->get();
        $noteAdmin = Role::whereNotIn('name',['admin'])->orderBy('id','desc')->get();
        return view("admin.roles.create",compact('titre','roles'));
    }

    public function store(Request $request){
        $validator = $request->validate(['name'=>['required','min:3']]);
        $role = Role::findByName(strtolower($request->name));
        if($role){
            return redirect()->back()->with( ['status' => 'error' , 'message' => 'Rôle déjà existe'] );
        }
        $role = new Role();
        $role->name = strtolower($request->name);
        $role->guard_name = "web";
        $role->save();
        return redirect()->back()->with( ['status' => 'success' , 'message' => 'Rôle créé avec succès'] );
    }

    public function edit (Role $role){
        $titre = "Attribuer Permissions";
        $modules = Module::all();
        return view("admin.roles.attribuer_permissions",compact('role','titre','modules'));
    }


    public function attribuer(Request $request , Role $role){
        $permissions = Permission::all();
        $role->revokePermissionTo($permissions);
        foreach ($request->permission as $key => $value) {
            $permission = Permission::find($value);
            if(!$role->hasPermissionTo($permission)){
                $permission->assignRole($role);
            }
        }
        return redirect()->route('admin.roles.index')->with( ['status' => 'success' , 'message' => 'Rôle Update avec succès'] );

    }
}
